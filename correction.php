<?php

include_once ("config.php");

if (suggest_correction($_POST['media'], $_POST['suggestion'], $_POST['submitter'])) {

    make_backup ();

    $success_notice = '<div class="alert alert-success" role="alert">Your suggested correction has been saved</div>';
} else {
    $success_notice = '<div class="alert alert-danger" role="alert">Error saving suggested correction</div>';
}

include ("index.php");

?>
