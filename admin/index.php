<?php

include_once ("../config.php");

$rows = get_admin_page_table ();
$corrections = get_corrections ();
$rss_items = get_rss_items ();

?><!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>The Queerbait-Tragicqueer-Cishet-comfort Test</title>

	<!-- Bootstrap -->
	<link href="<?php echo SITE_URL; ?>css/bootstrap.min.css" rel="stylesheet">

	<!-- QT css -->
	<link href="<?php echo SITE_URL; ?>qt.css" rel="stylesheet">

	<meta name="author" content="Benjamin Gregory Carlisle PhD Email: murph@bgcarlisle.com Website: https://www.bgcarlisle.com/ Social media: https://scholar.social/@bgcarlisle">

    </head>
    <body>
	<nav class="nav">
	    <a class="nav-link" href="<?php echo SITE_URL; ?>">Home</a>
	    <a class="nav-link" href="https://codeberg.org/bgcarlisle/QueerTestTracker" target="_blank">Source</a>
	    <a class="nav-link" href="https://scholar.social/@bgcarlisle" target="_blank">Social media</a>
	    <a class="nav-link" href="<?php echo SITE_URL; ?>admin/">Admin</a>
	</nav>
	<div id="citeMask" class="hidden" onclick="hide_dialog (event);">&nbsp;</div>
	<div id="addnew" class="notes">
	    <button class="btn btn-secondary btn-sm" style="float: right;" onclick="hide_dialog(event);">Close</button>
	    <h2>Add new entry</h2>
	    <form action="<?php echo SITE_URL; ?>admin/add-new.php" method="post">
		<div class="form-group">
		    <label for="type">Media type</label>
		    <input type="text" class="form-control" id="type" name="type" aria-describedby="textHelp">
		    <small id="textHelp" class="form-text text-muted">E.g. Film, Television series, Television programme</small>
		</div>
		<div class="form-group">
		    <label for="type">Title</label>
		    <input type="text" class="form-control" id="title" name="title" aria-describedby="titleHelp">
		    <small id="titleHelp" class="form-text text-muted">E.g. Star Trek: Deep Space Nine</small>
		</div>
		<div class="form-group">
		    <label for="type">Year</label>
		    <input type="text" class="form-control" id="year" name="year" aria-describedby="yearHelp">
		    <small id="yearHelp" class="form-text text-muted">E.-g. 1999</small>
		</div>
		<div class="form-group">
		    <label for="type">Details</label>
		    <input type="text" class="form-control" id="details" name="details" aria-describedby="detailsHelp">
		    <small id="detailsHelp" class="form-text text-muted">E.g. S07E14, Chimera</small>
		</div>
		<div class="form-group">
		    <label for="type">IMDB link</label>
		    <input type="text" class="form-control" id="imdb_link" name="imdb_link" aria-describedby="imdbHelp">
		</div>

		<div class="form-group">
		    <label for="queer_character">Is there at least one queer character?</label>
		    <select class="form-control" id="queer_character" name="queer_character">
			<option value="na"></option>
			<option value="1">At least one queer character</option>
			<option value="0">No queer characters</option>
		    </select>
		</div>

		<div id="queer_character_questions">

		    <div class="form-group">
			<label for="named">Does this queer character have a name?</label>
			<select class="form-control" id="named" name="named">
			    <option value="na"></option>
			    <option value="1">The queer character has a name</option>
			    <option value="0">The queer character is not named</option>
			</select>
		    </div>

		    <div class="form-group">
			<label for="name">What is the name of the queer character?</label>
			<input type="text" class="form-control" id="name" name="name" aria-describedby="nameHelp">
			<small id="nameHelp" class="form-text text-muted">E.g. Odo</small>
		    </div>

		    <div class="form-group">
			<label for="non_deniable">Would it be difficult to mis-read this character as cishet?</label>
			<select class="form-control" id="non_deniable" name="non_deniable">
			    <option value="na"></option>
			    <option value="1">Yes, this character is clearly queer</option>
			    <option value="0">No, this character is ambiguously queer</option>
			</select>
		    </div>
		    
		    <div class="form-group">
			<label for="blink_miss_it">Is this blink-and-you-miss-it "representation"?</label>
			<select class="form-control" id="blink_miss_it" name="blink_miss_it">
			    <option value="na"></option>
			    <option value="1">Blink-and-you-miss-it</option>
			    <option value="0">Non-blink-and-you-miss-it</option>
			</select>
		    </div>

		    <div class="form-group">
			<label for="tragic">Is this tragic queer representation?</label>
			<select class="form-control" id="tragic" name="tragic">
			    <option value="na"></option>
			    <option value="1">Tragic</option>
			    <option value="0">Non-tragic</option>
			</select>
		    </div>

		    <div class="form-group">
			<label for="cishet_comfort">Is this queer character mainly written for cishet comfort?</label>
			<select class="form-control" id="cishet_comfort" name="cishet_comfort">
			    <option value="na"></option>
			    <option value="1">Written for cishet comfort</option>
			    <option value="0">Not written for cishet comfort</option>

			</select>
		    </div>

		</div>

		<div class="form-group">
		    <label for="notes">Notes</label>
		    <textarea class="form-control" id="notes" name="notes" rows="5"></textarea>
		</div>

		<div class="form-group">
		    <label for="notes_contain_spoilers">Do these notes contain spoilers?</label>
		    <select class="form-control" id="notes_contain_spoilers" name="notes_contain_spoilers">
			<option value="na"></option>
			<option value="1">Yes, spoilers</option>
			<option value="0">No spoilers</option>
		    </select>
		</div>

		<button class="btn btn-block btn-primary" style="margin-bottom: 40px;">Save</button>
		
	    </form>
	</div>
	<?php foreach ($rows as $edit_dialog) {	?>
	    <div class="notes" id="edit-<?php echo $edit_dialog['id']; ?>">
		<button class="btn btn-secondary btn-sm" style="float: right;" onclick="hide_dialog(event);">Close without saving</button>
		<h2>Edit entry</h2>
		<form action="<?php echo SITE_URL; ?>admin/edit.php" method="post">
		    <input type="hidden" name="mid" value="<?php echo $edit_dialog['id']; ?>">
		    <div class="form-group">
			<label for="type">Media type</label>
			<input type="text" class="form-control" id="type" name="type" aria-describedby="textHelp" value="<?php echo str_replace ("\"", "&quot;", $edit_dialog['type']); ?>">
			<small id="textHelp" class="form-text text-muted">E.g. Film, Television series, Television programme</small>
		    </div>
		    <div class="form-group">
			<label for="type">Title</label>
			<input type="text" class="form-control" id="title" name="title" aria-describedby="titleHelp" value="<?php echo str_replace ("\"", "&quot;", $edit_dialog['title']); ?>">
			<small id="titleHelp" class="form-text text-muted">E.g. Star Trek: Deep Space Nine</small>
		    </div>
		    <div class="form-group">
			<label for="type">Year</label>
			<input type="text" class="form-control" id="year" name="year" aria-describedby="yearHelp" value="<?php echo $edit_dialog['year']; ?>">
			<small id="yearHelp" class="form-text text-muted">E.g. 1999</small>
		    </div>
		    <div class="form-group">
			<label for="type">Details</label>
			<input type="text" class="form-control" id="details" name="details" aria-describedby="detailsHelp" value="<?php echo str_replace ("\"", "&quot;", $edit_dialog['details']); ?>">
			<small id="detailsHelp" class="form-text text-muted">E.g. S07E14, Chimera</small>
		    </div>
		    <div class="form-group">
			<label for="type">IMDB link</label>
			<input type="text" class="form-control" id="imdb_link" name="imdb_link" aria-describedby="imdbHelp" value="<?php echo str_replace("\"", "&quot;", $edit_dialog['imdb_link']); ?>">
		    </div>

		    <div class="form-group">
			<label for="queer_character">Is there at least one queer character?</label>
			<select class="form-control" id="queer_character" name="queer_character">
			    <option value="na"<?php if (is_null($edit_dialog['queer_character'])) { echo " selected"; } ?>></option>
			    <option value="1"<?php if ($edit_dialog['queer_character'] == 1) { echo " selected"; } ?>>At least one queer character</option>
			    <option value="0"<?php if ($edit_dialog['queer_character'] == 0 & ! is_null($edit_dialog['queer_character'])) { echo " selected"; } ?>>No queer characters</option>
			</select>
		    </div>

		    <p>If you selected "No queer characters" above, please leave the remaining blank.</p>

		    <div class="form-group">
			<label for="named">Does this queer character have a name?</label>
			<select class="form-control" id="named" name="named">
			    <option value="na"<?php if (is_null($edit_dialog['named'])) { echo " selected"; } ?>></option>
			    <option value="1"<?php if ($edit_dialog['named'] == 1) { echo " selected"; } ?>>The queer character has a name</option>
			    <option value="0"<?php if ($edit_dialog['named'] == 0 & ! is_null ($edit_dialog['named'])) { echo " selected"; } ?>>The queer character is not named</option>
			</select>
		    </div>

		    <div class="form-group">
			<label for="name">What is the name of the queer character?</label>
			<input type="text" class="form-control" id="name" name="name" aria-describedby="nameHelp" value="<?php echo str_replace("\"", "&quot;", $edit_dialog['name']); ?>">
			<small id="nameHelp" class="form-text text-muted">E.g. Odo</small>
		    </div>

		    <div class="form-group">
			<label for="non_deniable">Would it be difficult to mis-read this character as cishet?</label>
			<select class="form-control" id="non_deniable" name="non_deniable">
			    <option value="na"<?php if (is_null($edit_dialog['non_deniable'])) { echo " selected"; } ?>></option>
			    <option value="1"<?php if ($edit_dialog['non_deniable'] == 1) { echo " selected"; } ?>>Yes, this character is clearly queer</option>
			    <option value="0"<?php if ($edit_dialog['non_deniable'] == 0 & ! is_null ($edit_dialog['non_deniable'])) { echo " selected"; } ?>>No, this character is ambiguously queer</option>
			</select>
		    </div>
		    
		    <div class="form-group">
			<label for="blink_miss_it">Is this blink-and-you-miss-it "representation"?</label>
			<select class="form-control" id="blink_miss_it" name="blink_miss_it">
			    <option value="na"<?php if (is_null($edit_dialog['blink_miss_it'])) { echo " selected"; } ?>></option>
			    <option value="1"<?php if ($edit_dialog['blink_miss_it'] == 1) { echo " selected"; } ?>>Blink-and-you-miss-it</option>
			    <option value="0"<?php if ($edit_dialog['blink_miss_it'] == 0 & ! is_null ($edit_dialog['blink_miss_it'])) { echo " selected"; } ?>>Non-blink-and-you-miss-it</option>
			</select>
		    </div>

		    <div class="form-group">
			<label for="tragic">Is this tragic queer representation?</label>
			<select class="form-control" id="tragic" name="tragic">
			    <option value="na"<?php if (is_null($edit_dialog['tragic'])) { echo " selected"; } ?>></option>
			    <option value="1"<?php if ($edit_dialog['tragic'] == 1) { echo " selected"; } ?>>Tragic</option>
			    <option value="0"<?php if ($edit_dialog['tragic'] == 0 & ! is_null ($edit_dialog['tragic'])) { echo " selected"; } ?>>Non-tragic</option>
			</select>
		    </div>

		    <div class="form-group">
			<label for="cishet_comfort">Is this queer character mainly written for cishet comfort?</label>
			<select class="form-control" id="cishet_comfort" name="cishet_comfort">
			    <option value="na"<?php if (is_null($edit_dialog['cishet_comfort'])) { echo " selected"; } ?>></option>
			    <option value="1"<?php if ($edit_dialog['cishet_comfort'] == 1) { echo " selected"; } ?>>Written for cishet comfort</option>
			    <option value="0"<?php if ($edit_dialog['cishet_comfort'] == 0 & ! is_null ($edit_dialog['cishet_comfort'])) { echo " selected"; } ?>>Not written for cishet comfort</option>
			</select>
		    </div>

		    <div class="form-group">
			<label for="notes">Notes</label>
			<textarea class="form-control" id="notes" name="notes" rows="5"><?php echo $edit_dialog['notes']; ?></textarea>
		    </div>

		    <div class="form-group">
			<label for="notes_contain_spoilers">Do these notes contain spoilers?</label>
			<select class="form-control" id="notes_contain_spoilers" name="notes_contain_spoilers">
			    <option value="na"<?php if (is_null($edit_dialog['notes_contain_spoilers'])) { echo " selected"; } ?>></option>
			    <option value="1"<?php if ($edit_dialog['notes_contain_spoilers'] == 1) { echo " selected"; } ?>>Yes, spoilers</option>
			    <option value="0"<?php if ($edit_dialog['notes_contain_spoilers'] == 0 & ! is_null ($edit_dialog['notes_contain_spoilers'])) { echo " selected"; } ?>>No spoilers</option>
			</select>
		    </div>

		    <button class="btn btn-block btn-primary" style="margin-bottom: 40px;">Save</button>
		    
		</form>
	    </div>
	<?php } ?>
	<?php foreach ($rows as $delete_dialog) { ?>
	    <div class="notes" id="delete-<?php echo $delete_dialog['id']; ?>">
		<h2>Delete <?php echo $delete_dialog['title']; ?> (<?php echo $delete_dialog['year']; ?>)?</h2>
		<?php if ( ! is_null ($delete_dialog['details']) ) { echo '<p style="font-style: italic;">' . $delete_dialog['details'] . '</p>'; } ?>
		<p>This cannot be undone.</p>
		<button class="btn btn-danger" onclick="delete_media(event, <?php echo $delete_dialog['id']; ?>);">Delete</button>
		<button class="btn btn-secondary" onclick="hide_dialog(event);">Do not delete</button>
	    </div>
	<?php } ?>
	<?php foreach ($rows as $rss_dialog) { ?>
	    <div class="notes" id="rss-<?php echo $rss_dialog['id']; ?>">
		<button class="btn btn-secondary btn-sm" style="float: right;" onclick="hide_dialog(event);">Close</button>
		<h2>Post <?php echo $rss_dialog['title']; ?> (<?php echo $rss_dialog['year']; ?>) to RSS?</h2>
		<?php if ( ! is_null ($rss_dialog['details']) ) { echo '<p style="font-style: italic;">' . $rss_dialog['details'] . '</p>'; } ?>
		<?php $already_posted = FALSE; ?>
		<div class="feedback">
		    <?php foreach ($rss_items as $rss_item) { if ( $rss_dialog['id'] == $rss_item['media_id']) { ?>
			<div style="margin-bottom: 20px;">
			    <p>This was posted to the RSS feed already (<?php echo $rss_item['when_posted']; ?>)</p>
			    <button class="btn btn-danger btn-sm" onclick="delete_rss(event, <?php echo $rss_item['id']; ?>, <?php echo $rss_dialog['id']; ?>);">Remove from RSS feed</button>
			</div>
			<?php $already_posted = TRUE; ?>
		    <?php } } ?>
		    <?php if (! $already_posted) { ?>
			<button class="btn btn-primary" onclick="post_rss(event, <?php echo $rss_dialog['id']; ?>, 'new');">Post as a new item</button>
		    <?php } else { ?>
			<button class="btn btn-primary" onclick="post_rss(event, <?php echo $rss_dialog['id']; ?>, 'edit');">Post current version as an update</button>
		    <?php } ?>
		</div>
	    </div>
	<?php } ?>
	<?php foreach ($corrections as $delcorr) { ?>
	    <div class="notes" id="delcorr-<?php echo $delcorr['id']; ?>">
		<h2>Delete suggestion correction</h2>
		<p>Submitter: <?php echo $delcorr['submitter']; ?></p>
		<p>Suggested correction: <?php echo str_replace("\n", "<br>", $delcorr['suggestion']); ?></p>
		<p>When submitted: <?php echo $delcorr['when_submitted']; ?></p>
		<button class="btn btn-danger" onclick="delete_correction(event, <?php echo $delcorr['id']; ?>);">Delete</button>
		<button class="btn btn-secondary" onclick="hide_dialog(event);">Do not delete</button>
	    </div>
	<?php } ?>
	<div class="container-fluid">
	    <div class="row">
		<div class="col">
		    <h1>Admin: The Queerbait-Tragicqueer-Cishet-comfort Test</h1>
		    <?php
		    echo $success_notice;
		    ?>
		    <button class="btn btn-primary" onclick="show_addnew(event);" style="margin-bottom: 40px;">Add new</button>
		</div>
	    </div>
	    <div class="row">
		<div class="col-md-6">
		    <div class="input-group" style="margin-bottom: 40px;">
			<input type="text" id="filter-table-input" class="form-control" placeholder="Filter table" aria-label="Filter table" aria-describedby="filter-table-button" value="<?php echo $_GET['s']; ?>">
			<div class="input-group-append">
			    <button class="btn btn-primary" type="button" id="filter-table-button" onclick="event.preventDefault();filter_table();">Filter table</button>
			    
			</div>
		    </div>
		</div>
		<div class="col-md-6">
		    <button class="btn btn-secondary btn-block" type="button" onclick="$('#filter-table-input').val('');event.preventDefault();filter_table();" style="margin-bottom: 40px;">Show all</button>
		</div>
	    </div>
	    <?php if ( count ($corrections) > 0 ) { ?>
		<div class="row">
		    <div class="col">
			<h2>Suggested corrections</h2>
		    </div>
		</div>
		<div class="row">
		    <div class="col">
			<table class="table table-striped table-hover table-sm">
			    <thead class="thead-light">
				<tr>
				    <th scope="col">Submitter</th>
				    <th scope="col">Correction</th>
				    <th scope="col" class="righttable">When submitted</th>
				    <th scope="col" class="righttable">Delete</th>
				</tr>
			    </thead>
			    <tbody>
				<?php foreach ( $corrections as $corr ) { ?>
				    <tr id="corr-<?php echo $corr['id']; ?>">
					<td><?php echo $corr['submitter']; ?></td>
					<td><?php echo str_replace ("\n", "<br>", $corr['suggestion']);  ?></td>
					<td class="righttable"><?php echo $corr['when_submitted']; ?></td>
					<td class="righttable"><button class="btn btn-sm btn-danger" onclick="delete_correction_prompt(event, <?php echo $corr['id']; ?>);">Delete</button></td>
				    </tr>
				<?php } ?>
			    </tbody>
			</table>
		    </div>
		</div>
	    <?php } ?>
	    <div class="row">
		<div class="col">
		    <h2>Media</h2>
		</div>
	    </div>
	    <div class="row">
		<div class="col">
		    <table class="table table-striped table-hover table-sm">
			<thead class="thead-light">
			    <tr>
				<th scope="col">Media</th>
				<th scope="col" class="righttable">Submitter</th>
				<th scope="col" class="righttable">Approved</th>
				<th scope="col" class="righttable">RSS</th>
				<th scope="col" class="righttable">Edit</th>
				<th scope="col" class="righttable">Delete</th>
			    </tr>
			</thead>
			<tbody>
			    <?php foreach ($rows as $row) { ?>

				<?php

				if (is_null($row['approved'])) {
				    $approved = '<td class="righttable" id="approved-' . $row['id'] .'" onclick="update_approved(event, ' . $row['id'] . ');">-</td>';
				} else {
				    if ($row['approved'] == 1) {
					$approved = '<td class="righttable table-success" id="approved-' . $row['id'] .'" onclick="update_approved(event, ' .$row['id'] . ');">Yes</td>';
				    } else {
					$approved = '<td class="righttable table-danger" id="approved-' . $row['id'] .'" onclick="update_approved(event, ' . $row['id'] . ');">No</td>';
				    }

				}

				?>
				<tr id="row-<?php echo $row['id']; ?>">
				    <th scope="row"><?php if ( ! is_null ( $row['imdb_link'] ) ) { ?><a href="<?php echo $row['imdb_link']; ?>" target="_blank"><?php } ?><?php echo $row['title']; ?><?php if ( ! is_null ( $row['year'] )) { ?> (<?php echo $row['year']; ?>)<?php } ?><?php if ( ! is_null ( $row['imdb_link'] ) ) { ?></a><?php } ?> <span class="badge badge-secondary"><?php echo $row['type']; ?></span><?php if ( ! is_null ( $row['details'] ) ) { ?><?php echo "<br>" . $row['details']; ?><?php } ?></th>
				    <td class="righttable"><?php echo $row['submitter']; ?></td>
				    <?php echo $approved; ?>
				    <td class="righttable"><button class="btn btn-sm btn-primary" onclick="rss_prompt(event, <?php echo $row['id']; ?>);">RSS</button></td>
				    <td class="righttable"><button class="btn btn-sm btn-secondary" onclick="edit_media(event, <?php echo $row['id']; ?>);">Edit</button></td>
				    <td class="righttable"><button class="btn btn-sm btn-danger" onclick="delete_prompt(event, <?php echo $row['id']; ?>);">Delete</button></td>
				</tr>
			    <?php } ?>
			</tbody>
		    </table>
		</div>
	    </div>
	</div>
	
	<!-- jQuery -->
	<script src="<?php echo SITE_URL; ?>jquery-3.4.1.min.js"></script>

	<!-- Popper.js -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.bundle.min.js"></script>

	<!-- Bootstrap JS -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.min.js"></script>

	<!-- QT JS -->
	<script>
	 function show_addnew (event) {
	     event.preventDefault();
	     $('#citeMask').fadeIn(400, function () {
		 $('#addnew').slideDown();
	     });
	 }
	 
	 function hide_dialog (event) {
	     event.preventDefault();
	     $('.notes').slideUp(0, function () {
		 $('#citeMask').fadeOut();
	     });
	 }

	 function delete_prompt (event, media_id) {
	     event.preventDefault();

	     $('#citeMask').fadeIn(400, function () {
		 $('#delete-' + media_id).slideDown();
	     });
	 }

	 function delete_correction_prompt (event, media_id) {
	     event.preventDefault();

	     $('#citeMask').fadeIn(400, function () {
		 $('#delcorr-' + media_id).slideDown();
	     });
	 }

	 function delete_media (event, media_id) {
	     event.preventDefault();

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>admin/delete.php',
		 type: 'post',
		 data: {
		     mid: media_id
		 },
		 dataType: 'html'
	     }).done ( function (response) {
		 if ( response != 'Error' ) {
		     $('#row-' + media_id).addClass('table-danger');
		     hide_dialog(event);
		     setTimeout (function () {
			 $('#row-' + media_id).remove();
		     }, 3000);
		 }
	     });
	 }

	 function delete_correction (event, correction_id) {
	     event.preventDefault();

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>admin/delete-correction.php',
		 type: 'post',
		 data: {
		     cid: correction_id
		 },
		 dataType: 'html'
	     }).done ( function (response) {
		 if ( response != 'Error' ) {
		     $('#corr-' + correction_id).addClass('table-danger');
		     hide_dialog(event);
		     setTimeout (function () {
			 $('#corr-' + correction_id).remove();
		     }, 3000);
		 }
	     });
	 }

	 function post_rss (event, media_id, post_type) {
	     event.preventDefault();

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>admin/post-rss.php',
		 type: 'post',
		 data: {
		     mid: media_id,
		     pt: post_type
		 },
		 dataType: 'html'
	     }).done ( function (response) {
		 $('#rss-' + media_id + ' .feedback').html(response);
	     });
	 }

	 function delete_rss (event, rss_id, media_id) {
	     event.preventDefault();

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>admin/delete-rss.php',
		 type: 'post',
		 data: {
		     rid: rss_id
		 },
		 dataType: 'html'
	     }).done ( function (response) {
		 $('#rss-' + media_id + ' .feedback').html(response);
	     });
	 }

	 function edit_media (event, media_id) {
	     event.preventDefault();
	     
	     hide_dialog(event);

	     $('#citeMask').fadeIn(400, function () {
		 $('#edit-' + media_id).slideDown();
	     });
	     
	 }

	 function rss_prompt (event, media_id) {
	     event.preventDefault();

	     hide_dialog(event);

	     $('#citeMask').fadeIn(400, function () {
		 $('#rss-' + media_id).slideDown();
	     });
	     
	 }

	 function update_approved (event, media_id) {

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>admin/approved.php',
		 type: 'post',
		 data: {
		     mid: media_id
		 },
		 dataType: 'html'
	     }).done ( function (response) {
		 if ( response != 'MySQL Error' ) {

		     $('#approved-' + media_id).html(response);

		     switch (response) {
			 case '-':
			     $('#approved-' + media_id).removeClass('table-danger table-success');
			     break;
			 case 'No':
			     $('#approved-' + media_id).removeClass('table-success');
			     $('#approved-' + media_id).addClass('table-danger');
			     break;
			 case 'Yes':
			     $('#approved-' + media_id).removeClass('table-danger');
			     $('#approved-' + media_id).addClass('table-success');
			     break;
		     }
		     
		 }
	     });
	     
	 }

	 function filter_table () {

	     query = $('#filter-table-input').val();

	     if ( query == '' ) {
		 $('tbody th').parent().fadeIn(0);
	     } else {
		 $('tbody th').parent().fadeOut(0);

		 $('tbody th').each( function (index) {

		     cell_value = $(this).html();

		     if (cell_value.toLowerCase().search(query.toLowerCase()) != -1) {
			 $(this).parent().fadeIn(0);
		     }
		     
		 });
		 
	     }
	     

	 }

	 $('#filter-table-input').on('input', function (e) {
	     filter_table ();
	 });

	 $(document).ready(function () {
	     if ( $('#filter-table-input').val() != '' ) {
		 filter_table();
	     }

	     $('#queer_character').on('change', function () {
		 if ($(this).val() == "0") {
		     $('#queer_character_questions').slideUp();
		 } else {
		     $('#queer_character_questions').slideDown();
		 }
	     });
	     
	 });

	</script>
    </body>
</html>
